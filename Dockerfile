FROM node:12-slim




WORKDIR /app/

# Installing dependencies
COPY package*.json /app/
RUN npm install

# Copying source files
COPY . /app/

# Building app


EXPOSE 8080

# Running the app
CMD [ "npm", "start"]